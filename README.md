# react-statements

[![NPM][npm svg]][npm package] [![Build status][build status svg]][build status] [![Code coverage][coverage svg]][jobs]

## Install

```bash
yarn add @alvarium/react-statements
```

## Usage

```tsx
import React, { Component } from 'react'

import If from '@alvarium/react-statements'

class Example extends Component {
  render () {
    return (
      <If condition={true}>
        <div>Works!</div>
      </If>
    )
  }
}
```

## License

[MIT © Alvarium.io](./LICENSE)

[npm svg]: https://img.shields.io/npm/v/@alvarium/react-statements.svg
[build status svg]: https://gitlab.com/alvarium.io/packages/react/react-statements/badges/master/pipeline.svg
[coverage svg]: https://gitlab.com/alvarium.io/packages/react/react-statements/badges/master/coverage.svg

[npm package]: https://www.npmjs.com/package/@alvarium/react-statements
[build status]: https://gitlab.com/alvarium.io/packages/react/react-statements/pipelines
[jobs]: https://gitlab.com/alvarium.io/packages/react/react-statements/-/jobs
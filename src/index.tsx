/**
 * @class If
 */

import * as React from 'react'

export type Props = {
  condition: boolean,

}

export default class If extends React.Component<Props> {
  render() {
    const { condition, children } = this.props

    if (!condition) {
      return null
    }

    return (
      <React.Fragment>
        { children }
      </React.Fragment>
    )
  }
}

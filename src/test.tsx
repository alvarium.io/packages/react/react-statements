import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import If from '.'

const props = {
  condition: true,
  children: <div className='test'>Test</div>,
}

Enzyme.configure({ adapter: new Adapter() })

describe('<If />', () => {
  it('renders without crashing', () => {
    const enzymeWrapper = shallow(<If {...props} />)
    expect(enzymeWrapper.exists()).toBe(true)
  })
  it('returns the children if passes the condition', () => {
    const enzymeWrapper = shallow(<If {...props} />)
    expect(enzymeWrapper.find('.test').exists()).toBe(true)
  })
  it('dosen\'t return the children if it dosen\'t passes the condition', () => {
    const thisProps = {
      ...props,
      condition: false,
    }
    const enzymeWrapper = shallow(<If {...thisProps} />)
    expect(enzymeWrapper.find('.test').exists()).toBe(false)
  })
})

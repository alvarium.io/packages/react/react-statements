import React, { Component } from 'react'

import If from 'react-statements'

export default class App extends Component {
  render () {
    return (
      <div>
        <If condition={true} >
          <div>Works!</div>
        </If>
      </div>
    )
  }
}
